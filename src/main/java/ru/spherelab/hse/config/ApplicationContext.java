package ru.spherelab.hse.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.beans.PropertyVetoException;

public class ApplicationContext {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    @ConfigurationProperties("application.properties")
    public ComboPooledDataSource createC3P0Pool() {
        ComboPooledDataSource source = new ComboPooledDataSource();
        DbProperties properties = new DbProperties();

        try {
            source.setDriverClass("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (PropertyVetoException e) {
            return null;
        }
        source.setPreferredTestQuery("SELECT 1 + 1");
        System.out.println(properties.getJdbcUrl());
        source.setJdbcUrl(properties.getJdbcUrl());
        source.setUser(properties.getJdbcUser());
        source.setPassword(properties.getJdbcPassword());
        source.setTestConnectionOnCheckin(true);
        source.setIdleConnectionTestPeriod(1);
        source.setCheckoutTimeout(0);

        return source;
    }

    @Bean
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(createC3P0Pool());
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(createC3P0Pool());
    }
}
