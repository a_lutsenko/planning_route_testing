package ru.spherelab.hse.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;

public class DbProperties {

    @Value("${spring.datasource.url}")
    @Getter
    public String jdbcUrl;

    @Value("${spring.datasource.username}")
    @Getter
    public String jdbcUser;

    @Value("${spring.datasource.password}")
    @Getter
    public String jdbcPassword;

}
