package ru.spherelab.hse.db.dao.types.devices.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.spherelab.hse.db.dao.types.devices.DeviceTypesDao;

@Repository
public class DeviceTypesDaoImpl implements DeviceTypesDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private String sql = "select dt.id from shcs.device_types dt where dt.name = :name";

    @Autowired
    public DeviceTypesDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Long getMobilePhoneTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("name", "мобильный телефон");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }
}
