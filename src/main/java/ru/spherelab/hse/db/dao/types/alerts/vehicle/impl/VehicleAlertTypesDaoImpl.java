package ru.spherelab.hse.db.dao.types.alerts.vehicle.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.spherelab.hse.db.dao.types.alerts.vehicle.VehicleAlertTypesDao;

@Repository
public class VehicleAlertTypesDaoImpl implements VehicleAlertTypesDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private String sql = "select at.id from shcs.alert_types at where at.code = :businessKey";

    @Autowired
    public VehicleAlertTypesDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Long getVehicleRouteAbsenceAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "VEHICLE_ROUTE_ABSENCE");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getSpeedViolationAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "VEHICLE_ROUTE_ABSENCE");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getVehicleRouteStartAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "VEHICLE_ROUTE_ABSENCE");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getLackOfCommunicationAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "LACK_OF_COMMUNICATION");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getVehicleDepositVisitedAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "DEPOSIT_VISITED");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

}
