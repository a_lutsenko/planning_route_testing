package ru.spherelab.hse.db.dao.types.alerts.helmet.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.spherelab.hse.db.dao.types.alerts.helmet.HelmetAlertTypesDao;

@Repository
public class HelmetAlertTypesDaoImpl implements HelmetAlertTypesDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private String sql = "select at.id from shcs.alert_types at where at.code = :businessKey";

    @Autowired
    public HelmetAlertTypesDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Long getHelmetHitAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "HELMET_HIT");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getHelmetRemovedAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "HELMET_REMOVED");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getHelmetFallDownAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "HELMET_FALL_DOWN");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getHelmetManDownAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "HELMET_MAN_DOWN");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getSeriousFallAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "SERIOUS_FALL");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

}
