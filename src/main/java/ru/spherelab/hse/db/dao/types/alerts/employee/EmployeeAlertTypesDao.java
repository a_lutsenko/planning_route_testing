package ru.spherelab.hse.db.dao.types.alerts.employee;

public interface EmployeeAlertTypesDao {

    Long getAlarmButtonAlertTypeId();

    Long getEmployeeRouteAbsenceAlertTypeId();

    Long getEmployeeRouteErrorAlertTypeId();

    Long getEmployeeSafexIncompleteAlertTypeId();

    Long getEmployeeMedexIncompleteAlertTypeId();

    Long getEmployeeWorkhoursExceededAlertTypeId();

    Long getEmployeeFellAsleepAlertTypeId();

    Long getLocationOvercrowedAlertTypeId();

    Long getEmployeeImmobilizedAlertTypeId();

    Long getEmployeeFellDownAlertTypeId();

    Long getEnterDangerZoneWithoutRespiratorAlertTypeId();

    Long getHeartRateAbnormalAlertTypeId();

    Long getEmployeeOutOfLocationAlertTypeId();
}
