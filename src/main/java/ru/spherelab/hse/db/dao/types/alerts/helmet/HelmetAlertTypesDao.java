package ru.spherelab.hse.db.dao.types.alerts.helmet;

public interface HelmetAlertTypesDao {

    Long getHelmetHitAlertTypeId();

    Long getHelmetRemovedAlertTypeId();

    Long getHelmetFallDownAlertTypeId();

    Long getHelmetManDownAlertTypeId();

    Long getSeriousFallAlertTypeId();
}
