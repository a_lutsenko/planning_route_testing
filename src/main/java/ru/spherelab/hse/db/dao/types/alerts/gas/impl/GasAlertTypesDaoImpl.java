package ru.spherelab.hse.db.dao.types.alerts.gas.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.spherelab.hse.db.dao.types.alerts.gas.GasAlertTypesDao;

@Repository
public class GasAlertTypesDaoImpl implements GasAlertTypesDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private String sql = "select at.id from shcs.alert_types at where at.code = :businessKey";

    @Autowired
    public GasAlertTypesDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Long getTlvExceededAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "TLV_EXCEEDED");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getHighCH4ConcentrationAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "HIGH_CH4_CONCENTRATION");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getHighO2ConcentrationAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "HIGH_O2_CONCENTRATION");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getHighH2SConcentrationAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "HIGH_H2S_CONCENTRATION");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getHighCOConcentrationAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "HIGH_CO_CONCENTRATION");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

}
