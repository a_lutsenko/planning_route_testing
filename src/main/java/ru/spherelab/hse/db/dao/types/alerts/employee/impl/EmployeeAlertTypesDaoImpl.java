package ru.spherelab.hse.db.dao.types.alerts.employee.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.spherelab.hse.db.dao.types.alerts.employee.EmployeeAlertTypesDao;

@Repository
public class EmployeeAlertTypesDaoImpl implements EmployeeAlertTypesDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private String sql = "select at.id from shcs.alert_types at where at.code = :businessKey";

    @Autowired
    public EmployeeAlertTypesDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Long getAlarmButtonAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "ALARM_BUTTON");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getEmployeeRouteAbsenceAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "EMPLOYEE_ROUTE_ABSENCE");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getEmployeeRouteErrorAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "EMPLOYEE_ROUTE_ERROR");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getEmployeeSafexIncompleteAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "EMPLOYEE_SAFEX_INCOMPLETE");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getEmployeeMedexIncompleteAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "EMPLOYEE_MEDEX_INCOMPLETE");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getEmployeeWorkhoursExceededAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "EMPLOYEE_WORKHOURS_EXCEEDED");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getEmployeeFellAsleepAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "EMPLOYEE_FELL_ASLEEP");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getLocationOvercrowedAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "LOCATION_OVERCROWDED");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getEmployeeImmobilizedAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "EMPLOYEE_IMMOBILIZED");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getEmployeeFellDownAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "EMPLOYEE_FELL_DOWN");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getEnterDangerZoneWithoutRespiratorAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "ENTER_DANGER_ZONE_WITHOUT_RESPIRATOR");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getHeartRateAbnormalAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "HEART_RATE_ABNORMAL");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getEmployeeOutOfLocationAlertTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "EMPLOYEE_OUT_OF_LOCATION");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }
}
