package ru.spherelab.hse.db.dao.types.events;

/**
 * Получение ID типов событий по бизнес-ключам.
 */
public interface EventTypesDao {

    Long getMonitoringEventTypeId();

    Long getLocationEventTypeId();

    Long getNavigationEventTypeId();

    Long getNonStructuredEventTypeId();

}
