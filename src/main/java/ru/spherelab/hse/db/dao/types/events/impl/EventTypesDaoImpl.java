package ru.spherelab.hse.db.dao.types.events.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.spherelab.hse.db.dao.types.events.EventTypesDao;


@Repository
public class EventTypesDaoImpl implements EventTypesDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private String sql = "select et.id from shcs.event_types et where et.code = :businessKey";

    @Autowired
    public EventTypesDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Long getMonitoringEventTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "MONITORING");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getLocationEventTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "LOCATION");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getNavigationEventTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "NAVIGATION");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);
    }

    @Override
    public Long getNonStructuredEventTypeId() {
        final SqlParameterSource param = new MapSqlParameterSource("businessKey", "NONSTRUCTURED");
        return jdbcTemplate.queryForObject(this.sql, param, Long.class);

    }
}
