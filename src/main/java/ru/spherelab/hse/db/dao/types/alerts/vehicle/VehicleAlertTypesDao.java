package ru.spherelab.hse.db.dao.types.alerts.vehicle;

public interface VehicleAlertTypesDao {

    Long getVehicleRouteAbsenceAlertTypeId();

    Long getSpeedViolationAlertTypeId();

    Long getVehicleRouteStartAlertTypeId();

    Long getLackOfCommunicationAlertTypeId();

    Long getVehicleDepositVisitedAlertTypeId();

}
