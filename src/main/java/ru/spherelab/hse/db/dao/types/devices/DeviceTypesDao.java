package ru.spherelab.hse.db.dao.types.devices;

public interface DeviceTypesDao {

    Long getMobilePhoneTypeId();
}
