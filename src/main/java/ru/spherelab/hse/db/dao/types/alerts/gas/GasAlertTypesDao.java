package ru.spherelab.hse.db.dao.types.alerts.gas;

public interface GasAlertTypesDao {

    Long getTlvExceededAlertTypeId();

    Long getHighCH4ConcentrationAlertTypeId();

    Long getHighO2ConcentrationAlertTypeId();

    Long getHighH2SConcentrationAlertTypeId();

    Long getHighCOConcentrationAlertTypeId();
}
